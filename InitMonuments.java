
/**
  *Groupe : F
  *Equipe : GENCTURCK & GUELINA
  */

import java.util.List;
import java.util.ArrayList;

public class InitMonuments
{
	public static List<Monument> init()
	{
		List<Monument> list = new ArrayList<>();
		
		list.add( new Monument( "Gare", "tour", "Vous pouvez lancer deux dés", 4 ) )                ;
		list.add( new Monument( "Centre commercial", "tour", 
			"Vos etablissements de type 'pain' et 'café' vous rapportent une piece de plus", 10 ) ) ;
		list.add( new Monument( "Tour radio", "tour", 
			"Une fois par tour, vous pouvez choisir de relancer vos dés", 22 ) )                    ;
		list.add( new Monument( "Parc d'attractions", "tour", 
			"Si votre jet de dé est double, rejouez un tour apres celui-ci", 16 ) )                 ;
			
		return list ;	
	}

	public static Monument getMonument( String nom )
	{
		for( Monument monu : InitMonuments.init() )
		{
			if( monu.getCarte().equals( nom ) )
			{
				return monu ;
			}
		}
		return  null ;
	}
}
