/**
  *Groupe : F
  *Equipe : GENCTURCK & GUELINA
  */

import java.util.*;

public class MiniVille 
{
	
	private Pioche   pioche             ;
	private Banque   banque             ;
	private Joueur[] tabJoueurs         ;  
	private int      indexJoueurCourant ;
	private int      resultatLanceDe1   ;
	private int      resultatLanceDe2   ;


	public MiniVille(int nbJoueur)
	{
		this.pioche             = new Pioche();
		this.banque             = new Banque();
		this.tabJoueurs         = new Joueur[nbJoueur];
		this.indexJoueurCourant = 0;
		
		//On instancie les joueurs et on distribue
		//les monuments et etablissements de depart
		for(int i=0; i<nbJoueur; i++)
		{
			this.tabJoueurs[i] = new Joueur();
			this.distributionDeDepart( this.tabJoueurs[i] );
		}	
		
	}


	public boolean estNomCarteValide(String nomCarte)
	{
		return this.pioche  .getCarte   (nomCarte) != null || 
		       InitMonuments.getMonument(nomCarte) != null ;
	}

	public Pioche getPioche()
	{
		return this.pioche;
	}

	public Joueur[] getJoueurs()
	{
		return this.tabJoueurs;
	}
	
	public Joueur getJoueurCourant()
	{
		return this.tabJoueurs[this.indexJoueurCourant];
	}

	/** Change l'indexJoueurCourant au prochain joueur */
	public void joueurSuivant()
	{
		int nbJoueur = this.tabJoueurs.length;
		this.indexJoueurCourant = (this.indexJoueurCourant+1) % nbJoueur ;
	}


	public boolean finDePartie()
	{
		for( Joueur joueur : this.tabJoueurs )
			if( joueur.tousMonumentsConstruits() )
				return true;
		return false;
	}
	
	
	/** Simule un lancé de dé */
	public int lancerDe()
	{
		int jet = (int)(Math.random()*6)+1;
		return jet                         ;
	}
	
	/** Simule un lancé de dé avec deux dés */
	public int lancerDeuxDes()
	{
		this.resultatLanceDe1 = this.lancerDe()              ;
		this.resultatLanceDe2 = this.lancerDe()              ;
		
		return this.resultatLanceDe1 + this.resultatLanceDe2 ;
	}

	/** Distribue au joueur donné les monuments et
	  * les cartes de départ
	  */
	public void distributionDeDepart( Joueur joueur )
	{
		// Etablissements de depart
		for( Etablissement et : InitCartesDeDepart.init() )
		{
			joueur.addEtablissement( et )        ;
		}
		// Monuments
		for( Monument monu : InitMonuments.init() )
		{
			joueur.addMonument( monu )            ;
		}
 	}
	
	//
	public void gestionEffets( int jet, Joueur joueur)
	{
		GestionDesEffets.gererEffetsMonuments      ( joueur                          ) ; 
		GestionDesEffets.gererEffetsCartesRouges   ( jet, joueur, tabJoueurs, banque ) ;           
		GestionDesEffets.gererEffetsCartesVertes   ( jet, joueur, banque             ) ;
		GestionDesEffets.gererEffetsCartesViolettes( jet, joueur, tabJoueurs         ) ;
		GestionDesEffets.gererEffetsCartesBleues   ( jet, tabJoueurs, banque         ) ;
		GestionDesEffets.regulateur                ( joueur                          ) ;
	}
	

	//
	public boolean estDouble()
	{
		return this.resultatLanceDe1 == this.resultatLanceDe2 ;
	}

	// Achat
	public boolean gestionAchats( String reponse, Joueur joueur, String nomCarte )
	{
		boolean achatSucces = false ;
		
		if( reponse.equals("OUI") )
		{
			Etablissement et = pioche.retirerEtablissement( nomCarte )                  ;
			if( et != null )
			{
				if( joueur.getPieces() >= et.getCout() )
				{	
					joueur.retirerPieces( et.getCout() ) ;
					banque.ajouterPieces( et.getCout() )          ;
					joueur.addEtablissement( et )                         ;
					achatSucces = true;
				}
			}
		}
		
		else if( reponse.equals("OUI M") )
		{
			for( Monument monu : joueur.getMonument() )
			{
				if( (monu.getCarte().equals(nomCarte)) && ( joueur.getPieces() >= monu.getCout() ) )
				{
					monu.setConstruction( false )                           ;
					joueur.retirerPieces( monu.getCout() )                  ;
					banque.ajouterPieces( monu.getCout() )          ;
					achatSucces = true;
				}
			}
		}
		return achatSucces ;
	}
}	
