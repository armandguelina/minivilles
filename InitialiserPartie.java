
/**
  *Groupe : F
  *Equipe : GENCTURCK & GUELINA
  */

import java.util.*;
import java.io.*;

public class InitialiserPartie
{
	public static Controleur charger( Controleur ctrl, String nomFichier )
	{
		try
		{
			FileReader fr = new FileReader(nomFichier) ;
			Scanner    sc = new Scanner   ( fr ) ;
			
			int nbJoueurs = Integer.valueOf( sc.nextLine() ) ;
			ctrl          = new Controleur( nbJoueurs ) ;

	 		while( sc.hasNext() )
			{
				String line = sc.nextLine() ;
				int numJoueur = Integer.valueOf( line );
				Joueur joueur = ctrl.getMetier().getJoueurs()[numJoueur-1] ;

				String[] tabNomMonuments = sc.nextLine().split(";");
				for( String nom : tabNomMonuments )
				{
					for( Monument monuJoueur : joueur.getMonument() )
					{
						if( monuJoueur.getCarte().equals(nom) )
						{
							monuJoueur.setConstruction(false);
						}
					}
				}
				
				String[] tabNomEtablissements = sc.nextLine().split(";");
				for( String nom : tabNomEtablissements )
				{
					Etablissement etab = ctrl.getMetier().getPioche().retirerEtablissement( nom ) ;
					if( etab != null )
					{
						joueur.addEtablissement( etab ) ;
					}
				}
				joueur.setPieces(Integer.valueOf( sc.nextLine() ));
				sc.nextLine() ;
			}	
		}
		catch( Exception e )
		{
			e.printStackTrace();
		} 
		
		return ctrl;
	} 
}
