/**
  *Groupe : F
  *Equipe : GENCTURCK & GUELINA
  */
  
import java.util.*;

public abstract class Etablissement
{
    protected int[]  resultatJet;
    protected String nomCarte               ;
    protected String symbole                ;
    protected String effet                  ;
	protected String couleur                ;
    protected int cout                      ;
	protected final int gainDefaut          ;
	protected int       gain                ;
	protected int       gain1             ;
    
    public Etablissement  (String couleur, String nomCarte, String symbole, 
	                       String effet, int cout, int gain,int gain1, int[] resultatJet)
    {
		this.couleur     = couleur                 ;
        this.resultatJet = resultatJet             ;

        this.nomCarte    = nomCarte                ;
        this.symbole     = symbole                 ;
        this.effet       = effet                   ;  
        this.cout        = cout                    ;
		this.gainDefaut  = gain                    ;
		this.gain1       = gain1  ;
    }
	
	public int getGain1()
	{
		return this.gain1 ;
	}
	public String getCouleur ()
	{ 
		return this.couleur;
	}
	
	public int getGainDefaut()
	{
		return this.gainDefaut;
	}
	
	public int getGain()
	{
		return this.gain;
	}
	
	public void setGain( int gain )
	{ 
		this.gain = gain;
	}

    public int getResultat ()
    { 
		return this.resultatJet[0];
    }
    
    public boolean contientJet(int jet)
    {
    	for(int jetPossible : this.resultatJet)
    		if( jetPossible == jet )
    			return true;
    			
		return false;
    }
    
    public int getCout ()
    {
       return this.cout;
    }
    
    public String getEffet ()
    {
       return this.effet;
    }

    public String getSymbole ()
    {
       return this.symbole;
    }
    
    public String getCarte ()
    {
       return this.nomCarte;
    }
	   
	
	public String getJetString()
	{
		String sRet = "";
		
		for( int i=0; i<this.resultatJet.length; i++ )
		{
			sRet += String.format("%d" ,resultatJet[i]) ;
			sRet += (i != this.resultatJet.length-1)? "-" : "" ;
		}
		
		return sRet;
	}

	public String toString()
	{
		String str ;
		
		str = String.format("%-28s | %-8s | %-9s | jet : %5s | cout : %2d",
		            nomCarte, couleur, symbole, this.getJetString(), cout );
		
		return str ;
	}
}
