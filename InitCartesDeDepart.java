/**
  *Groupe : F
  *Equipe : GENCTURCK & GUELINA
  */
  
import java.util.*;

public class InitCartesDeDepart
{
	public static List<Etablissement> init()
	{
		List<Etablissement> list = new ArrayList<>() ;
		
		list.add( new EtablissementDepart("BLEUE", "Champs de ble", "ble", 
			"Pendant le tour de n'importe quel joueur, recevez 1 pièce de la banque", 0, 1, 1, new int[]{1} ) ) ;
		list.add( new EtablissementDepart("VERTE", "Boulangerie", "pain", 
			"Pendant votre tour uniquement, recevez 1 pièce de la banque", 0, 1, 1, new int[]{2,3}) )           ; 
			
		return list;
	}
}
