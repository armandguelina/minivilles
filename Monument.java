
/**
  *Groupe : F
  *Equipe : GENCTURCK & GUELINA
  */


public class Monument 
{
	private String nomCarte          ;
    private String symbole           ;
    private String effet             ;
    private int cout                 ;
    private boolean estEnConstruction;
    
    public Monument(String nomCarte, String symbole, String effet, int cout)
    { 
		this.nomCarte = nomCarte     ;
        this.symbole  = symbole      ;
        this.effet    = effet        ;
        this.cout     = cout         ;
        this.estEnConstruction = true;
    }
    
    public void setConstruction( boolean bool )
    {  
       this.estEnConstruction = bool;
    }
    
	public boolean getConstruction()
    {  
       return this.estEnConstruction;
    }
	
    public int getCout ()
    {
       return this.cout;
    }
    
    public String getEffet ()
    {
       return this.effet;
    }

    public String getSymbole ()
    {
       return this.symbole;
    }
    
    public String getCarte ()
    {
       return this.nomCarte;
    } 
	
	
	public String toString()
	{		
		return String.format("%-18s | %4s | cout : %2d", this.nomCarte, this.symbole, this.cout);
	}
	
}
