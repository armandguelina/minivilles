
/**
  *Groupe : F
  *Equipe : GENCTURCK & GUELINA
  */


public class Banque
{
	private final int NB_PIECE_DEPART = 54 ;
	private int       nbPieces ;
	
	public Banque()
	{
		this.nbPieces = NB_PIECE_DEPART ;
	}

	public int getTotalPiece()
	{
		return this.nbPieces ;
	}

	public boolean ajouterPieces(int pieces)
	{
		if( pieces <= 0 )
		{
			return false; 
		}

		this.nbPieces += pieces ;
		return true ;
	}


	public boolean retirerPieces(int pieces)
	{
		if( this.nbPieces - pieces >= 0 )
		{
			this.nbPieces -= pieces ;
			return true ;
		}
		return false;
	} 
 }


