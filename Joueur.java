/**
  *Groupe : F
  *Equipe : GENCTURCK & GUELINA
  */
  
import java.util.*;

public class Joueur
{
	private static int num                            ;
	private int        numero                         ;
	private ArrayList<Etablissement> ensEtablissement ;
	private ArrayList<Monument>      ensMonument      ;
	private int pieces                                ;
	
	public Joueur()
	{
		this.numero            = ++Joueur.num      ;
		this.ensMonument       = new ArrayList<>() ;
		this.ensEtablissement  = new ArrayList<>() ;
		this.pieces            = 20                ;
	}
	
	public int getNumero()
	{
		return this.numero;
	}
	
	public int getPieces()
	{
		return this.pieces;
	}
	
	public List<Etablissement> getDepart()
	{
		List<Etablissement> ensDepart = new ArrayList<>();

		for( Etablissement etablissement : this.ensEtablissement )
		{
			if( etablissement instanceof EtablissementDepart )
			{
				ensDepart.add( etablissement );
			}
		}
		return ensDepart ;
	}
	
	public List<Monument> getMonument()
	{
		return this.ensMonument ;		
	}
	
	public List<Etablissement> getBase()
	{
		List<Etablissement> ensBase = new ArrayList<>();
		
		for( Etablissement etablissement : this.ensEtablissement )
		{
			if( etablissement instanceof EtablissementBase )
			{
				ensBase.add( etablissement );
			}
		}
		return ensBase ;
	}
	
	public List<EtablissementSpecial> getSpecial()
	{
		List<EtablissementSpecial> ensSpecial = new ArrayList<>();

		for( Etablissement etablissement : this.ensEtablissement )
		{
			if( etablissement instanceof EtablissementSpecial )
			{
				ensSpecial.add( (EtablissementSpecial)etablissement );
			}
		}
		return ensSpecial ;
	}
	
	public void setPieces( int pieces )
	{
		this.pieces = pieces;
	}

	public void ajouterPieces( int pieces )
	{
		this.pieces += pieces ;
	}
	
	public boolean retirerPieces( int pieces )
	{
		if( this.pieces - pieces > 0 )
		{
			this.pieces -= pieces ;
			return true ;
		}
		return false;
	}
	
	public void addMonument( Monument monu )
	{
		this.ensMonument.add( monu );
	}

	public boolean tousMonumentsConstruits()
	{
		for( Monument monument :  this.ensMonument )
			if( monument.getConstruction() )
				return false;
		return true;
	}

	public void addEtablissement(Etablissement etablissement)
	{
		if( etablissement == null )
		{
			return;
		}
		
		if( etablissement instanceof EtablissementSpecial )
		{
			if( !this.getSpecial().contains( (EtablissementSpecial)etablissement ) )
			{
				this.ensEtablissement.add(etablissement);
			}
		}
		else
		{
			this.ensEtablissement.add(etablissement);
		}
	}
	
	
	public Etablissement retirerEtablissement( Etablissement et )
	{
		for( int i = 0; i< this.ensEtablissement.size(); i++ )
		{
			if( this.ensEtablissement.get(i) == et )
			{
				this.ensEtablissement.remove(i);
				return et                      ;
			}
		}
		return null ;
	}
	
	
	
	public boolean estMonumentConstruit( String nomMonument )
	{
		for( Monument monu : this.ensMonument )
		{
			if( monu.getCarte().equals(nomMonument) && !monu.getConstruction() )
			{
				return true ;
			}
		}
		return false ;
	}
	
	
	public String toString()
	{
		String str = "";
		
		String ensEtDepart      = "";
		String ensEtBase        = "";
		String ensEtSpecial     = "";
		String ensMonuConstruit = "";
		int    nbMonuConstruit  = 0 ;
		
		for( Etablissement  etab : this.getDepart()  )
			ensEtDepart  += "\t" + etab.toString() + "\n";
		
		for( Etablissement  etab : this.getBase()    )
			ensEtBase    += "\t" + etab.toString() + "\n";
			
		for( Etablissement  etab : this.getSpecial() )
			ensEtSpecial += "\t" + etab.toString() + "\n";
		
		for( Monument monu : this.ensMonument )
		{
			if( !monu.getConstruction() )
			{
				ensMonuConstruit += "\t" + monu.toString() + "\n";
				nbMonuConstruit++;
			}
		}
			
		
		str += "Joueur n°"   + this.numero +  "\n" + 
		       "\tPIECES : " + this.pieces +  "\n" +
		       "\tETABLISSEMNTS DE DEPART : " + this.getDepart ().size() + "\n" +
		          ensEtDepart  + "\n" +
			   "\tETABLISSEMENTS DE BASE  : " + this.getBase   ().size() + "\n" + 
			      ensEtBase    + "\n" +  
		       "\tETABLISSEMENTS SPECIAUX : " + this.getSpecial().size() + "\n" +
		          ensEtSpecial + "\n" +
		       "\tMONUMENTS CONSTRUITS    : " + nbMonuConstruit          + "\n" + 
				  ensMonuConstruit    + "\n" ;

		return str;
	}
	
	
}
