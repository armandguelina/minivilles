/**
  *Groupe : F
  *Equipe : GENCTURCK & GUELINA
  */

import java.util.*;

public class Controleur
{
	private static boolean MODE_DEBUG = true;

	private MiniVille metier ;
	private IHMCUI    ihm    ;

	public Controleur( int nbJoueurs )
	{
		this.metier = new MiniVille( nbJoueurs    ) ;
		this.ihm    = new IHMCUI   ( this         ) ; 
	}


	public MiniVille getMetier()
	{
		return this.metier ;
	}
	
	/** Donne des details sur une carte donnée( Monument ou Etablissement ) */
	public void detaillerCarte()
	{
		String reponse ;

		reponse = this.ihm.lireReponse("Voulez-vous détailler une carte ? ( pour un monument 'OUI M' et pour tout autre achat 'OUI' )").toUpperCase();
		
		if( reponse.matches("(O|OUI|OUI M)") )
		{
			do
			{
				reponse = this.ihm.lireReponse("Entrer le nom de la carte à détailler : ");
				this.ihm.detaillerCarte(reponse);
			}while( !this.metier.estNomCarteValide(reponse) );
		}
	}


	public void processusAchat( Joueur joueur )
	{
		String reponse ;
		do
		{
			reponse = this.ihm.lireReponse("Voulez-vous effectuer un achat ? " +
			 ": ( pour un monument 'OUI M' et pour tout autre achat 'OUI' ) ");	
			reponse = reponse.trim().toUpperCase();
		}while( !reponse.matches("([ON]|OUI|NON|OUI M)") );
		
		if( reponse.equals("O") || reponse.equals("OUI") || reponse.equals("OUI M") )
		{
			String nomCarte = this.ihm.lireReponse("Veillez préciser le nom de la carte : ");
			boolean achatSucces = this.metier.gestionAchats(reponse, joueur, nomCarte) ;
			
			if( !achatSucces ) this.ihm.afficher("Fonds insuffisants!");
			else               this.ihm.afficher("Achat réalisé!"     );
		}
	}


	public static void main(String[] args)
	{
		System.out.print("Voulez-vous charger une partie ? : " ) ;
		Scanner sc = new Scanner( System.in ) ;
		String str = sc.nextLine().toUpperCase() ;
		Controleur ctrl  = null ;
		
		if( str.matches("(O|OUI)"))
		{
			System.out.print("Nom du fichier : ") ;
			str = sc.nextLine() ; 
			ctrl = InitialiserPartie.charger( ctrl, str ) ;
			System.out.println(ctrl);
		}
		else
		{
			ctrl = new Controleur(2) ;
		}

		Joueur joueurCourant    = ctrl.metier.getJoueurCourant();
		int    numJoueurCourant = joueurCourant.getNumero     ();
		String reponse ;
		int    lanceDe ;

		
		while( !ctrl.metier.finDePartie() )
		{		
			ctrl.ihm.afficherJoueurs   ();
			//ctrl.ihm.afficherBanque    ();
			ctrl.ihm.afficherPioche    ();	
			ctrl.ihm.afficherMonuments (joueurCourant);		
			ctrl.ihm.afficher("Joueur n°" + numJoueurCourant + " à votre tour!");	
			
			if( MODE_DEBUG )
			{
				lanceDe = ctrl.ihm.lireReponseInt("MODE DEBUG\nEntrer le jet : ");
			}
			else if( joueurCourant.estMonumentConstruit("Gare") )
			{
				reponse = ctrl.ihm.lireOuiOuNon("Voulez-vous jouer avec 2 dés ? ") ;
				if( reponse.equals("O") || reponse.equals("OUI") )
					lanceDe = ctrl.metier.lancerDeuxDes() ;
				else
					lanceDe = ctrl.metier.lancerDe();
			}
			else // On lance un dé
			{
				lanceDe = ctrl.metier.lancerDe() ;
			}


			ctrl.ihm.afficherJet(numJoueurCourant, lanceDe);

			// Gère l'effet Parc d'attractions
			if( ctrl.metier.estDouble()  && 
			    joueurCourant.estMonumentConstruit("Tour radio") )
			{
				
				ctrl.ihm.afficher("Effet : Tour radio!");
				reponse = ctrl.ihm.lireOuiOuNon("Voulez-vous relancer les dés ? ");
				if( reponse.equals("O") || reponse.equals("OUI") )
				{
					lanceDe = ctrl.metier.lancerDeuxDes() ;
					ctrl.ihm.afficherJet(numJoueurCourant, lanceDe);
				}
				
			}
			
			// Déclencher les effets			
			ctrl.metier.gestionEffets(lanceDe, joueurCourant);

			// Détailler carte et processus d'achat 
			ctrl.detaillerCarte() ;
			ctrl.processusAchat(joueurCourant) ;

			// Gère l'effet Tour radio
			if( joueurCourant.estMonumentConstruit("Parc d'attractions") )
			{
				ctrl.ihm.afficher("Vous possédez Parc d'attractions !");
				reponse = ctrl.ihm.lireOuiOuNon("Voulez-vous rejouer une seconde fois ? " ) ;

				if( reponse.equals("OUI") || reponse.equals("O") )
				{
					lanceDe = ctrl.metier.lancerDeuxDes() ;
					ctrl.ihm.afficherJet( joueurCourant.getNumero(), lanceDe ) ;
					ctrl.metier.gestionEffets(lanceDe, joueurCourant);
					ctrl.detaillerCarte() ;
					ctrl.processusAchat(joueurCourant) ;
				}
				
			}
			
			

			ctrl.metier.joueurSuivant() ;		
			joueurCourant    = ctrl.metier.getJoueurCourant();
			numJoueurCourant = joueurCourant.getNumero     ();	
		}
		
		ctrl.ihm.afficher("Fin de partie !");
		ctrl.ihm.afficher("Victoire du joueur n°" + (numJoueurCourant-1) ) ;
	}

}
