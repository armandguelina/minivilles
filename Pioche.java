/**
  *Groupe : F
  *Equipe : GENCTURCK & GUELINA
  */
  
import java.util.*;

public class Pioche
{
	
   private ArrayList<Etablissement> pioche ;
   
   public Pioche()
   {	   
	   this.pioche = new ArrayList<>() ;
	   this.initCartes()               ;
   }
   

   public void initCartes( )
   {
	   final int NB_CARTE_BASE = 6 ;	
		
		this.addEtablissement(NB_CARTE_BASE,
		    new EtablissementBase("BLEUE", "Champs de ble", "ble", 
			"Pendant le tour de n'importe quel joueur, recevez 1 pièce de la banque", 1, 1, 1, new int[]{1}) );
			
		this.addEtablissement(NB_CARTE_BASE, 
		    new EtablissementBase("BLEUE", "Ferme", "vache", 
			"Pendant le tour de n'importe quel joueur, recevez 1 pièce de la banque", 1, 1, 1, new int[]{2}) );
			
		this.addEtablissement(NB_CARTE_BASE, 
		    new EtablissementBase("VERTE", "Boulangerie", "pain", 
			"Pendant votre tour uniquement, recevez 1 pièce de la banque", 1, 1, 1, new int[]{2,3}) );	
			
		this.addEtablissement(NB_CARTE_BASE, 
		     new EtablissementBase("ROUGE", "Cafe", "cafe", 
			"Recevez 1 pièce du joueur qui a lancé les dès", 2, 1, 1, new int[]{3}) );
			
		this.addEtablissement(NB_CARTE_BASE, 
		    new EtablissementBase("VERTE", "Superette", "pain", 
			"Pendant votre tour uniquement, recevez 3 pièces de la banque", 2, 3, 3, new int[]{4}) );
			
		this.addEtablissement(NB_CARTE_BASE, 
		    new EtablissementBase("BLEUE", "Foret", "engrenage", 
			"Pendant le tour de n'importe quel joueur, recevez 1 pièce de la banque", 3, 1, 1, new int[]{5}) );
			
		this.addEtablissement(NB_CARTE_BASE, 
		    new EtablissementBase("VERTE", "Fromagerie", "usine", 
			"Pendant votre tour uniquement, recevez 3 pièces de la banque pour " + 
			"chaque établissement de type 'vache' que vous possédez", 5, 3, 3, new int[]{7}) );
			
		this.addEtablissement(NB_CARTE_BASE, 
		    new EtablissementBase("VERTE", "Fabrique de meubles", "usine", 
			"Pendant votre tour uniquement, recevez 3 pièces de la banque " +
			"pour chaque établissement de type 'engrenage' que vous possédez", 3, 3, 3, new int[]{8}) );
			
		this.addEtablissement(NB_CARTE_BASE, 
		    new EtablissementBase("BLEUE", "Mine", "engrenage", 
			"Pendant le tour de n'importe quel joueur, recevez 5 pièce de la banque", 6, 5,5, new int[]{9}) );
			
		this.addEtablissement(NB_CARTE_BASE, 
		    new EtablissementBase("ROUGE", "Restaurant", "cafe", 
			"Recevez 2 pièces du joueur qui a lancé les des", 3, 2,2, new int[]{9,10}) );	
			
		this.addEtablissement(NB_CARTE_BASE, 
		    new EtablissementBase("BLEUE", "Verger", "ble", 
			"Pendant le tour de n'importe quel joueur, recevez 3 pièce de la banque", 1, 3,3, new int[]{10}) );
			
		this.addEtablissement(NB_CARTE_BASE,
		    new EtablissementBase("VERTE", "Marche de fruits et legumes", "fruit", 
			"Pendant votre tour uniquement, recevez 2 pièces de la banque " + 
			"pour chaque établissement de type 'ble' que vous possédez", 2, 2,2, new int[]{11,12}));
			
			
			
		/* Etablissements speciaux */
		
		final int NB_CARTE_SPECIAL = 4;
	
		this.addEtablissement( NB_CARTE_SPECIAL, 
		 new EtablissementSpecial("VIOLETTE", "Stade", "tour", 
	     "Pendant votre tour uniquement, recevez 2 pièces de la part chaque autre joueur", 6, 2,2, new int[]{6}) );
		 
		this.addEtablissement( NB_CARTE_SPECIAL, 
		    new EtablissementSpecial("VIOLETTE", "Chaine de television", "tour", 
			"Pendant votre tour uniquement, recevez 5 pièces du joueur de votre choix", 7, 5,5, new int[]{6}) ); 
			
		this.addEtablissement( NB_CARTE_SPECIAL, 
		    new EtablissementSpecial("VIOLETTE", "Centre d'affaires", "tour", 
			"Pendant votre tour uniquement, vous pouvez echanger avec le  joueur de votre choix " +  
			"un etablissement qui ne soit du type tour", 8, 0, 0, new int[]{6}) );
   
   }
   

	private void addEtablissement(int nbFois, Etablissement etab)
	{
		for(int i=0; i<nbFois; i++)
			this.pioche.add( etab );
	}
   
	public Etablissement getCarte( String nomCarte )
	{
		Etablissement etTmp = null ;
		
		for( Etablissement et : this.pioche )
		{
			if( et.getCarte().equals(nomCarte) )
			{
				etTmp = et ;
			}
		}
		
		return etTmp;
	}

	public Etablissement retirerEtablissement( String nomCarte )
	{
		Etablissement etab = this.getCarte( nomCarte ) ;
		if( etab != null )
		{
			this.pioche.remove( etab );
		}
		return etab ;
	}

	
	public int getStock(String nomCarte)
	{
		int stock  = 0;
		
		for( Etablissement etab : this.pioche )
			if( etab.getCarte().equals(nomCarte) )
				stock++;
		return stock;
	}
   
  
	public List<Etablissement> getEnsEtUnique()
	{
		List<Etablissement> ensEtUnique = new ArrayList<>();
		ensEtUnique.add( this.pioche.get(0) );
		
		boolean canAdd = false;
		
		for( Etablissement toAdd : this.pioche )
		{
			canAdd = true ;
			for( Etablissement etab : ensEtUnique )
			{
				if( toAdd.getCarte().equals(etab.getCarte()) )
				{
					canAdd = false;
				}
			}
			
			if( canAdd ) ensEtUnique.add( toAdd );
		}
		return ensEtUnique;
	}
	
	public String detaillerCarte(Etablissement etab)
	{
		if( etab == null ) return "";

		return  "Nom     : " + etab.getCarte    () + "\n" + 
		        "Couleur : " + etab.getCouleur  () + "\n" +
		        "Symbole : " + etab.getSymbole  () + "\n" +
		        "Jet     : " + etab.getJetString() + "\n" +
		        "Cout    : " + etab.getCout     () + "\n" +
		        "Effet   : " + etab.getEffet    () + "\n"  ;
	}

	
	public String detaillerMonument(Monument monu)
	{
		if( monu == null ) return "";

		return  "Nom     : " + monu.getCarte    () + "\n" + 
		        "Symbole : " + monu.getSymbole  () + "\n" +
		        "Cout    : " + monu.getCout     () + "\n" +
		        "Effet   : " + monu.getEffet    () + "\n"  ;
	}
	
	public String toString()
	{
		String str = "";

		str += "*-------------------------------------------------------------------------------*\n"
		+ "*------------------------------CARTES EN RESERVE--------------------------------*\n"
		+ "*-------------------------------------------------------------------------------*\n\n\n" ;
		
		
		for( Etablissement etab : this.getEnsEtUnique() )
		{
			int stock = this.getStock(etab.getCarte());
			str += String.format("%02d x ", stock) +  etab.toString() + "\n" ;
		}

		return str;
	}
	
}
