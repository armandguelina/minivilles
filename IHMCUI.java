import java.util.*;
//import iut.algo.*;


public class IHMCUI
{
	private Controleur ctrl;
		
	public IHMCUI(Controleur ctrl)
	{
		this.ctrl   = ctrl;
	}

	public void afficherJoueurs()
	{
		System.out.println("Joueurs : ");
	
		for( Joueur joueur : this.ctrl.getMetier().getJoueurs() )
			System.out.println( "\t" + joueur );
			
		System.out.println();	
	}
	
	public void afficher(String message)
	{
		System.out.println(message + "\n");
	}

	public String lireReponse(String message)
	{
		Scanner sc = new Scanner(System.in);
		
		System.out.print(message);
		
		return sc.nextLine();
	}
	
	public int lireReponseInt(String message)
	{
		Scanner sc = new Scanner(System.in);
		
		System.out.print(message);
		
		try               { return sc.nextInt();            }
		catch(Exception e){ return lireReponseInt(message); }
	}

	public String lireOuiOuNon(String message)
	{
		String reponse ;
		
		do
		{
			reponse = this.lireReponse(message + "[O]ui [N]on : ");
			reponse = reponse.trim().toUpperCase();
		}while( !reponse.matches("([ON]|OUI|NON)") );
		
		return reponse;
	}

	public int lireNbJoueurs()
	{
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Entrer le nombre de joueur (min-> 2 | max-> 4) : ");
		
		try               { return sc.nextInt()    ; }
		catch(Exception e){ return lireNbJoueurs() ; }		
	}

	public void afficherPioche()
	{
		System.out.println("Pioche : \n");
		System.out.println( this.ctrl.getMetier().getPioche() );
		System.out.println();
	}

	public void afficherMonuments(Joueur joueur)
	{
		String affichage = "";
		String etatMonu  = "";
		
		for( Monument monu : joueur.getMonument() )
		{
			if( joueur.estMonumentConstruit(monu.getCarte()) ) etatMonu = "CONSTRUIT";
			else                                               etatMonu = "A CONSTRUIRE";
			
			affichage += String.format("%-13s | ", etatMonu) + monu.toString() + "\n";
		}
		
		System.out.println( affichage );
	}
	
	public void detaillerCarte(String nomCarte)
	{
		if( this.ctrl.getMetier().estNomCarteValide(nomCarte) )
		{
			if( this.ctrl.getMetier().getPioche().getCarte( nomCarte ) != null )
			{
				Etablissement etab = this.ctrl.getMetier().getPioche().getCarte( nomCarte );
				System.out.println( this.ctrl.getMetier().getPioche().detaillerCarte(etab) );
			}
			else
			{
				Monument monu = InitMonuments.getMonument( nomCarte ) ;
				System.out.println( this.ctrl.getMetier().getPioche().detaillerMonument(monu) ) ;
			}
		}
		else
		{
			System.out.println("Nom de carte invalide !");
		}
	}


	public void afficherJet( int numJoueur, int lanceDe )
	{
		System.out.println("Lancée de dé pour joueur n°" + numJoueur + " ...");
		System.out.println("Joueur n°" + numJoueur + " vous avez obtenu un " + lanceDe);
	}

}
