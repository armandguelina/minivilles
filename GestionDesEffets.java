/**
  *Groupe : F
  *Equipe : GENCTURCK & GUELINA
  */
  
import java.util.*;
//import iut.algo.*;

public class GestionDesEffets
{	
	public static void gererEffetsMonuments( Joueur joueur )
	{
		List<Monument> al = joueur.getMonument();
		for( Monument monu : al )
		{
			if( (monu.getConstruction() == false) && (monu.getCarte().equals("Centre Commercial")) )
			{
				List<Etablissement> l = joueur.getBase();
				for( Etablissement et : l )
				{
					if( (et.getSymbole().equals("tasse")) || (et.getSymbole().equals("pain")) )
					{
						et.setGain( et.getGain() + 1 );
					}
				}
			}
		}
	}
	
	public static void regulateur( Joueur joueur )
	{
		List<Monument> al = joueur.getMonument();
		for( Monument monu : al )
		{
			if( (monu.getConstruction() == false) && (monu.getCarte().equals("Centre Commercial")) )
			{
				List<Etablissement> l = joueur.getBase();
				for( Etablissement et : l )
				{
					if( (et.getSymbole().equals("tasse")) || (et.getSymbole().equals("pain")) )
					{
						et.setGain( et.getGain() - 1 );
					}
				}
			}
		}
	}
	
	
	public static void gererEffetsCartesVertes( int jet, Joueur joueur, Banque banque )
	{
		/*List<Etablissement> al = joueur.getBase();
		al.add( joueur.getDepart().get(1) )      ;
		for( Etablissement et : al )
		{
			if( et.contientJet(jet) && et.getCouleur().equals("VERTE") )
			{
				GestionDesEffets.gestionDesTransactionsEntreBanqueEtJoueur( et, banque, joueur ) ;
			}
		}*/
		List<Etablissement> al = joueur.getBase();
		al.add( joueur.getDepart().get(1) )      ;
		
		for( Etablissement et : al )
		{
			// Si ce n'est pas une carte VERTE ou si le jet ne correspond pas 
			// à celui de la carte, on passe à l'établissement suivant
			if( !et.contientJet(jet) || !et.getCouleur().equals("VERTE") ) 
				continue;		
				
				
			/* A partir d'ici toutes les cartes sont vertes et correspondent au jet */
		
			// Chaque carte de symbole 'ble' rapporte 3 pièces de +
			if( et.getCarte().equals("Marche de fruits et legumes") )
			{
				int nbBle = GestionDesEffets.getNbCartesAvecSymbole("ble", al);
				et.setGain( nbBle * et.getGainDefaut() );
			}
			// Chaque carte de symbole 'engrenage' rapporte 3 pièces de +
			else if( et.getCarte().equals("Fabrique de meubles") )
			{
				int nbEngrenage = GestionDesEffets.getNbCartesAvecSymbole("engrenage", al);
				et.setGain( nbEngrenage * et.getGainDefaut() );
			}
			// Chaque carte de symbole 'vache' rapporte 3 pièces de +
			else if( et.getCarte().equals("Fromagerie") )
			{
				int nbVache = GestionDesEffets.getNbCartesAvecSymbole("vache", al);
				et.setGain( nbVache * et.getGainDefaut() );
			}
			
			// On applique les transactions
			GestionDesEffets.gestionDesTransactionsEntreBanqueEtJoueur( et, banque, joueur ) ;
		}
	}
	
	
	public static int getNbCartesAvecSymbole(String symbole, List<Etablissement> liste)
	{
		int nb = 0;
		
		for( Etablissement etab : liste )
			if( etab.getSymbole().equals(symbole) )
				nb++;
		
		return nb;
	}


	public static void gererEffetsCartesBleues( int jet, Joueur[] tabJoueurs, Banque banque )
	{		
		for( Joueur joueur : tabJoueurs )
		{
			List<Etablissement> al = joueur.getBase();
			al.add( joueur.getDepart().get(0) );
			for( Etablissement et : al )
			{
				if( et.contientJet(jet) && (et.getCouleur().equals("BLEUE")) )
				{
					GestionDesEffets.gestionDesTransactionsEntreBanqueEtJoueur( et, banque, joueur ) ;
				}
			}
		}
	}
	
	
	public static void gererEffetsCartesRouges( int jet, Joueur joueur, Joueur[] tabJoueurs, Banque banque )
	{		
		for( Joueur jr : tabJoueurs )
		{
			if( jr.getNumero() != joueur.getNumero() )
			{
				List<Etablissement> list = jr.getBase();
				
				for( Etablissement et: list )
				{
					if( et.contientJet(jet)  && (et.getCouleur().equals("ROUGE")) )
					{
						GestionDesEffets.gestionDesTransactionsEntreJoueurs( et, joueur, jr ) ;
					}
				}
			}
		}
	}
	
	
	public static void gererEffetsCartesViolettes( int jet, Joueur joueur, Joueur[] tabJoueurs )
	{
		
		List<EtablissementSpecial> al = joueur.getSpecial();
		for( Etablissement et : al )
		{
			if( (et.getCout() == 6) && ( jet == 6) )
			{
				for( Joueur jr : tabJoueurs )
				{
					if( jr.getNumero() != joueur.getNumero() )
					{
						GestionDesEffets.gestionDesTransactionsEntreJoueurs( et, jr, joueur) ;
					}
				}
			}
			
			if( (et.getCout() == 7) && ( jet == 6) )
			{
				System.out.println("Veillez indiquer le numero du joueur de qui vous voulez recevoir vos 5 pièces :" );
				int choix = /*Clavier.lire_int()*/new Scanner(System.in).nextInt();
				if( (choix >=0) && (choix < tabJoueurs.length) && (tabJoueurs[choix] != null) && (tabJoueurs[choix].getNumero() != joueur.getNumero()) )
				{
					GestionDesEffets.gestionDesTransactionsEntreJoueurs( et, tabJoueurs[choix], joueur ) ;
				}
				else
				{
					System.out.println("Numero du joueur non valide ");
				}
			}
			
			if( (et.getCout() == 8) && ( jet == 6) )
			{
				System.out.println("Veillez indiquer le numero du joueur avec qui vous voulez proceder à l'échange :" ) ;
				int choix = /*Clavier.lire_int()*/new Scanner(System.in).nextInt()                                                                          ;
				
				if( (choix >=0) && (choix < tabJoueurs.length) && (tabJoueurs[choix] != null) && (tabJoueurs[choix].getNumero() != joueur.getNumero()) )
				{
					Etablissement etLocuteur      = null;
					Etablissement etInterlocuteur = null;
					
					for( int i = 0; i< tabJoueurs.length; i++ )
					{
						if( ( tabJoueurs[i].getNumero() == joueur.getNumero() ) )
						{
							etLocuteur = GestionDesEffets.carteADonner( tabJoueurs[i] ) ;
						}
						
						if(  tabJoueurs[i].getNumero() == tabJoueurs[choix].getNumero() )
						{
							etInterlocuteur = GestionDesEffets.carteADonner( tabJoueurs[i] ) ;
						}
					}
					
					if( (!etLocuteur.equals(null)) && (!etInterlocuteur.equals(null)) )
					{
						joueur.addEtablissement( tabJoueurs[choix].retirerEtablissement(etInterlocuteur) );
						tabJoueurs[choix].addEtablissement( joueur.retirerEtablissement(etLocuteur) )     ;
					}
					else
					{
						System.out.println(" Carte(s) non valide(s), transaction echouée " );
					}
				}
				else
				{
					System.out.println("Numero du joueur non valide ");
				}
			}
		}
		
	}
	
	
	public static Etablissement carteADonner( Joueur joueur )
	{
		System.out.println("Indication du nom de la carte à échanger :") ;
		String nom                    = /*Clavier.lireString()*/new Scanner(System.in).nextLine()             ;
		List<Etablissement> lBase = joueur.getBase()                 ;
		for( Etablissement etBase : lBase )
		{
			if( (etBase.getCarte().equals(nom)) )
			{
				return etBase ;
			}	
		}
		return null ;
	}
	
	
	public static void gestionDesTransactionsEntreJoueurs( Etablissement et, Joueur donneur, Joueur receveur )
	{
		boolean bool = true;
		bool = donneur.retirerPieces(et.getGain1());
		if( bool )
		{
			receveur.ajouterPieces(et.getGain1());
		}
		else
		{
			System.out.println("Fonds insuffisants ");
		}
	}
	
	
	public static void gestionDesTransactionsEntreBanqueEtJoueur( Etablissement et, Banque banque, Joueur joueur )
	{
		boolean bool = true;
		bool = banque.retirerPieces(et.getGain1());
		if( bool )
		{
			joueur.ajouterPieces(et.getGain1());
		}
		else
		{
			System.out.println("Fonds insuffisants ");
		}
	}

}
